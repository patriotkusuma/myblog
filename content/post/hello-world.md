---
title: "C++, Apa Sih Itu?"
date: 2018-11-03T13:28:12+07:00
linktitle: C++, Apa Sih Itu?
author: "Patriot Kusuma Sejati"
weight: 10
categories: "programming, c++"
image: "uploads/bg1.jpeg"

---
# C++


### Sejarah Singkat Bahasa C
___

Bahasa pemrograman C++ dikembangkan di _Bell lab_ pada awal tahun 1970-an. Bahasa itu diturunkan dari bahasa sebelumnya, yaitu *B* yang diturunkan dari bahasa sebelumnya, yaitu *BCL*. Awalnya, bahasa tersebut dirancang sebagai bahasa pemrograman yang dijalankan pada sistem operasi **UNIX**. Pada perkembangannya, versi **ANSI** (_American National Standard Institue_) C menjadi versi yang dominan. Meskipun versi tersebut sekarang jarang dipakai dalam pengembangan-pengembangan baru, tetapi versi itu masih banyak dipakai dalam beberapa pengembangan sistem dan jaringan maupun untuk sistem embedded.

### Sejarah Singkat Bahasa C++
___

*Bjarne Stroustrup* pada _Bell Labs_ pertama kali mengembangkan C++ pada awal 1980-an. Untuk mendukung fitur-fitur pada C++, dibangun efisiensi dan sistem support untuk pemrogramman tingkat rendah (_low level coding_). Pada C++ ditambahkan konsep-konsep baru seperti class dengan sifat-sifatnya seperti _inheritance_ dan _overloading_.

### Perbedaan Antara C dan C++
___
meskipun bahasa-bahasa tersebut menggunakan sintaks yang sama, tetapi mereka memiliki perbedaan. **C** merupakan bahasa pemrograman _prosedural_, di mana penyelesaian atas suatu masalah dilakukan dengan membagi-bagi masalah tersebut ke dalam sub-submasalah yang lebih kecil. Selain itu, **C++** merupakan bahasa pemrograman yang memiliki sifat **Object Oriented Programming (_OOP_)**. Untuk menyelesaikan masalah, C++ melakukan langkah pertama dengan mendefinisikan class-class yang merupakan class yang dibuat sebelumnya sebagai abstraksi dari objek-objek fisik. Class tersebut berisi keadaan objek, anggota-anggotanya, dan kemampuan dari objeknya. Setelah beberapa Class dibuat, masalah dipecahkan menggunakan class.

#### Contoh Program dengan C++

```C++
#include <iostream>     
void main()
{
    cout << "Ini adalah program pertamaku\n";
    return 0;
}
```

#### Keterangan
1. Line 1 : ```#include <iostream>```
    Sebagai bagian dari proses compile, compiler dari C++ menjalankan program yang dinamakan preprosesor. Preprosesor memiliki kemampuan menambahkan dan menghapus kode dari file sumber. Pada contoh ini, directive ```#include``` memberitahu preposesor untuk menyertakan kode dari file iostream. File iostream tersebut berisi deklarasi untuk berbagai fungsi yang dibutuhkan oleh program, atau class-class yang dibutuhkan.
2. Line 2 : ```void main()```
    Pernyataan itu mendeklarasikan fungsi utama, bahwa suatu program C++ dapat berisi banyak fungsi, tetapi harus selalu memiliki sebuah fungsi utama (_main function_). Fungsi adalah modul yang berisi kode untuk menyelesaikan masalah-masalah tertentu. Kala _void_ menandakan fungsi main tidak bertipe.
3. Line 3 : ```{```
    Kurung kurawal buka menandakan awal program.
4. Line 4 : ```cout << "Ini adalah program pertamaku\n;```
    *Cout* adalah sebuah objek dari library standar C++ yang digunakan untuk mencetak string ke peranti output standar, yang biasanya adalah layar komputer kita. Compiler menghubungkan kode dari library standar itu dengan kode yang telah kita tuliskan untuk mendapatkan hasil yang executable. Tanda ``\n`` adalah formal modifier yang digunakan untuk berganti baris setelah menampilkan string. Jika ada cout lain pada program tersebut, maka string yang menyertainya akan ditulikan pada baris dibawahnya.
5. Line 5 : ```}```
    Kurung kurawal tutup menandakan akhir program.